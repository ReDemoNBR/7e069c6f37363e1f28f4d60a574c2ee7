import loader from "./loader";

const env = loader();

export const {
    POSTGRES_HOST,
    POSTGRES_PORT,
    POSTGRES_DB,
    POSTGRES_USER,
    POSTGRES_PASSWORD,
    DB_MIN_CONNECTIONS,
    DB_MAX_CONNECTIONS,
    DB_IDLE_TIME,
    DB_ACQUIRE_TIME,
    DB_CHECK_INTERVAL_CONNECTIONS,

    SERVER_API_HOST,
    SERVER_API_PORT,
    DEFAULT_LIMIT,
    DEFAULT_MAX_LIMIT,
    API_HEADER_NAME,
    MAX_REQUEST_BODY_SIZE,
    API_HEADER_VALUE,
    API_TIMEOUT,

    PROCESS_WORKERS_COUNT,
    NODE_ENV,
    NODE_CLUSTER_SCHED_POLICY,
    TZ,
    PROD,
    LOG_LEVEL
} = env;

export default env;
