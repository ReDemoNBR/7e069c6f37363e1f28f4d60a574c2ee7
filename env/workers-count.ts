import OS from "os";
const {abs, min} = Math;

export default (workersCount: number | string = "upto-4")=>{
    let processCount = abs(parseInt(workersCount.toString())) || workersCount;
    if (processCount && typeof processCount==="string") {
        processCount = processCount.toLowerCase();
        // eslint-disable-next-line node/global-require
        const threads = OS.cpus().length || 1; // added at least one thread because some android phones CPUs are not detected and cpus().length = 0
        if (processCount==="all") processCount = threads;
        else if (processCount.startsWith("upto-")) {
            processCount = abs(parseInt(processCount.replace("upto-", ""))) || 4;
            processCount = min(processCount, threads);
        } else processCount = min(threads, 4);
    }
    return processCount as number;
};
