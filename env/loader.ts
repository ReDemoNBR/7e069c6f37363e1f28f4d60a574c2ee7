import "dotenv/config";
import Bytes from "bytes";
import ms from "ms";
import workersCount from "./workers-count";

const {abs} = Math;

// eslint-disable-next-line complexity
export default ()=>{
    const {env} = process;
    return Object.freeze({
        // DATABASE
        POSTGRES_HOST: env.POSTGRES_HOST ?? "localhost",
        POSTGRES_PORT: parseInt(env.POSTGRES_PORT ?? "5432"),
        POSTGRES_DB: env.POSTGRES_DB ?? "redacted",
        POSTGRES_USER: env.POSTGRES_USER ?? "redacted_user",
        POSTGRES_PASSWORD: env.POSTGRES_PASSWORD ?? "redacted_password",
        DB_MIN_CONNECTIONS: abs(parseInt(env.DB_MIN_CONNECTIONS ?? "1")),
        DB_MAX_CONNECTIONS: abs(parseInt(env.DB_MAX_CONNECTIONS ?? "25")),
        DB_IDLE_TIME: ms(env.DB_IDLE_TIME ?? "10 seconds"),
        DB_ACQUIRE_TIME: ms(env.DB_ACQUIRE_TIME ?? "60 seconds"),
        DB_CHECK_INTERVAL_CONNECTIONS: ms(env.DB_CHECK_INTERVAL_CONNECTIONS ?? "3 seconds"),

        // API SERVICE
        SERVER_API_HOST: env.SERVER_API_HOST ?? "http://localhost",
        SERVER_API_PORT: abs(parseInt(env.PORT ?? env.SERVER_API_PORT ?? "7000")),
        DEFAULT_LIMIT: abs(parseInt(env.DEFAULT_LIMIT ?? "10")),
        DEFAULT_MAX_LIMIT: abs(parseInt(env.DEFAULT_MAX_LIMIT ?? "1000")),
        API_HEADER_NAME: env.API_HEADER_NAME ?? "X-Redacted-Name-Version",
        MAX_REQUEST_BODY_SIZE: Bytes.parse(env.MAX_REQUEST_BODY_SIZE ?? "500KB"),
        API_HEADER_VALUE: env.API_HEADER_VALUE ?? "0.1.0",
        API_TIMEOUT: ms(env.API_TIMEOUT ?? "30 seconds"),

        // PROCESS
        PROCESS_WORKERS_COUNT: workersCount(env.PROCESS_WORKERS_COUNT ?? "upto-4"),
        NODE_ENV: env.NODE_ENV,
        NODE_CLUSTER_SCHED_POLICY: env.NODE_CLUSTER_SCHED_POLICY || "rr",
        TZ: env.TZ ?? "Etc/UTC",
        PROD: env.NODE_ENV==="production",
        LOG_LEVEL: env.LOG_LEVEL ?? "info"
    });
};
