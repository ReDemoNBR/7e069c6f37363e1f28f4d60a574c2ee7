import {fn, DataTypes} from "sequelize";
import db from "../";

export default db.define("user", {
    id: {
        type: DataTypes.BIGINT,
        autoIncrement: true,
        autoIncrementIdentity: true,
        primaryKey: true,
        unique: true,
        allowNull: false
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    phone: DataTypes.STRING, // removed NOT NULL for assessment purposes
    address: {
        type: DataTypes.JSON,
        allowNull: false
    }
}, {
    indexes: [
        {fields: ["email"], using: "HASH"},
        {fields: ["phone"], using: "HASH"},
        {fields: [fn("LOWER", "name")], name: "lower_name"}
    ],
    tableName: "user"
});
