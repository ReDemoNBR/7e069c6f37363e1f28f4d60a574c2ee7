import {Sequelize, Transaction} from "sequelize";
import logger from "./logger";
import env from "../../env";

export default new Sequelize({
    host: env.POSTGRES_HOST,
    port: env.POSTGRES_PORT,
    database: env.POSTGRES_DB,
    username: env.POSTGRES_USER,
    password: env.POSTGRES_PASSWORD,
    dialect: "postgres",
    logging: logger,

    benchmark: true,
    timezone: env.TZ,
    isolationLevel: Transaction.ISOLATION_LEVELS.READ_COMMITTED,
    define: {
        timestamps: true,
        createdAt: "created",
        updatedAt: "updated",
        deletedAt: "deleted",
        paranoid: false,
        underscored: true,
        freezeTableName: true
    },
    pool: {
        max: env.DB_MAX_CONNECTIONS,
        min: env.DB_MIN_CONNECTIONS,
        idle: env.DB_IDLE_TIME,
        acquire: env.DB_ACQUIRE_TIME,
        evict: env.DB_CHECK_INTERVAL_CONNECTIONS
    }
});
