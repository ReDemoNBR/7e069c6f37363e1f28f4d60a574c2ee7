import ms from "ms";
import Logger from "../logger";

export default (message: string, benchmark = 0)=>Logger.verbose("%s; Benchmark: %s;", message.replace(/;$/, ""), ms(benchmark));
