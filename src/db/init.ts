import pg from "pg";
import sequelize from "./";
import "./models/user"; // orphaned table

// In PG, PostgreSQL BIGINT is casted to string, this converts it to ES BigInt
pg.types.setTypeParser(20, (value: string)=>BigInt(value));

export default ()=>sequelize.sync();
