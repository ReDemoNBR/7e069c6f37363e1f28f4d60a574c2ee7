import Cluster from "cluster";
import {promisify} from "util";
import exit from "./cluster/exit";
import fork from "./cluster/fork";
import listening from "./cluster/listening";
import online from "./cluster/online";
import {PROCESS_WORKERS_COUNT} from "../env";

async function close() {
    await promisify(Cluster.disconnect)();
}

/** @TODO remove Cluster.isMaster when NodeJS v14 support is dropped */
if ((Cluster.isPrimary || Cluster.isMaster) && !Cluster.isWorker) {
    Cluster.on("exit", exit);
    Cluster.on("fork", fork);
    Cluster.on("listening", listening);
    Cluster.on("online", online);
    process.once("SIGTERM", close);
    process.once("SIGINT", close);

    for (let i=0; i<PROCESS_WORKERS_COUNT; i++) Cluster.fork();
}
