import {Application} from "express";

export default (app: Application)=>{
    app.set("etag", "strong");
    app.set("json replacer", (_key: string, value: any)=>typeof value==="bigint" ? value.toString() : value);
    app.set("json spaces", 0);
    app.set("query parser", "simple");
    app.set("trust proxy", true);
    return app;
};
