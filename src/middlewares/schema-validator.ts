import {Request, Response, NextFunction} from "express";
import createError from "http-errors";
import {mixed, ObjectSchema, ValidationError} from "yup";
const allowedKeys = ["body", "headers", "params", "query"];

export type SchemaValidatorOptionsType = {
    noUnknown?: boolean;
    /* pick?: string[];
    omit?: string[];
    required?: boolean;*/
};

export type KeyType = "body" | "headers" | "params" | "query";

function combineSchemas(schemaList: ObjectSchema<any>[], options: SchemaValidatorOptionsType) {
    // eslint-disable-next-line unicorn/prefer-spread
    let schema = schemaList.flat().reduce((combined, schema)=>combined = combined.concat(schema));

    schema = schema.required();
    if (options.noUnknown!==false) schema = schema.noUnknown();
    /* @NOTE not used */
    /*
    if (Array.isArray(options.pick)) schema = schema.pick(options.pick);
    if (Array.isArray(options.omit)) schema = schema.omit(options.omit);
    if (options.required) schema = schema.required();
    */

    return schema;
}

type OptsType = SchemaValidatorOptionsType | ObjectSchema<any>;

function schemaValidator(key: KeyType, opts: OptsType, ...schemaList: ObjectSchema<any>[]) {
    if (!allowedKeys.includes(key)) throw TypeError(`Invalid key ${key}. It must be one of the following: ${allowedKeys.join(", ")}`);
    let options = {noUnknown: true};

    // tests if is is array (ex: schemaValidator("body", [sch1, sch2]...)) or instance of yup schema (ex: schemaValidator("body", sch1, ...))
    const isOptions = !Array.isArray(opts) && !(opts instanceof mixed);
    schemaList = schemaList.flat();
    if (!isOptions) schemaList.unshift(opts as ObjectSchema<any>);
    else options = {...options, ...opts};

    const combinedSchema = combineSchemas(schemaList, options);

    return async(req: Request, res: Response, next: NextFunction)=>{
        try {
            /* eslint-disable security/detect-object-injection */
            const original = req[key];
            // eslint-disable-next-line require-atomic-updates
            req[key] = await combinedSchema.validate(req[key]);
            res.locals[key] = original;
            /* eslint-enable security/detect-object-injection */
            if (key==="body" && !Object.keys(req.body).length)
                return next(createError(400, "invalid body", {errors: ["body has no valid fields"]}));
        } catch(e) {
            if (e instanceof ValidationError)
                return next(createError(400, `invalid ${key==="params" ? "route params" : key}`, {errors: e.errors}));
            return next(e);
        }
        return next();
    };
}

export default schemaValidator;
export const bodyValidator = schemaValidator.bind(null, "body");
export const headerValidator = schemaValidator.bind(null, "headers");
export const paramsValidator = schemaValidator.bind(null, "params");
export const queryValidator = schemaValidator.bind(null, "query");
