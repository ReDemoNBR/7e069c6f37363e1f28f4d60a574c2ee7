import morgan from "morgan";
import Logger from "../logger";

const format = ":method :url :status :response-time[0] ms :remote-addr :referrer :user-agent";

export default morgan(format, {stream: Logger.morganHttpStream});
