import Cluster from "cluster";
import {Request, Response, NextFunction} from "express";
import Logger from "../logger";

const {worker} = Cluster;

export default ()=>{
    if (!worker) return function clusterLog(_req: Request, _res: Response, next: NextFunction) {
        return next();
    };
    return function clusterLog(req: Request, _res: Response, next: NextFunction) {
        Logger.debug(`cluster ${worker.id} (pid: ${process.pid}) responding to '${req.originalUrl}'`);
        return next();
    };
};
