import {Request, Response, NextFunction} from "express";
import createError from "http-errors";

export default function accepts(req: Request, _res: Response, next: NextFunction) {
    if (!req.accepts("json")) return next(createError(406, "Client must accept JSON responses"));
    return next();
}
