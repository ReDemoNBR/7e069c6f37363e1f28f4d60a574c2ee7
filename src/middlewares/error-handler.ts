import {Request, Response, NextFunction} from "express";
import createError from "http-errors";
import Logger from "../logger";

export type CustomErrorType = Error & {
    errors?: string[];
};

export type HttpErrorOutputType = {
    error: true;
    message: string;
    errors?: string[];
};

export function unhandledError(error: CustomErrorType | string, req: Request, res: Response, _next: NextFunction) {
    Logger.warn(`Error happened on ${req.originalUrl}`, error);
    let httpError;
    if (createError.isHttpError(error)) httpError = error;
    else {
        const status = !res.statusCode || res.statusCode===200 ? 500 : res.statusCode;
        let message;
        if (typeof error==="string") message = error;
        else if (error.message && typeof error.message==="string" && error.name && typeof error.name==="string")
            message = error;
        else message = "Unhandled server error";
        httpError = createError(status, message);
    }
    const errorObject: HttpErrorOutputType = {
        error: true,
        message: httpError.message
    };
    if (typeof error!=="string" && error.errors && Array.isArray(error.errors) && error.errors.length)
        errorObject.errors = error.errors.filter(message=>message && typeof message==="string");
    return res.status(httpError.status).send(errorObject);
}

export default unhandledError;
