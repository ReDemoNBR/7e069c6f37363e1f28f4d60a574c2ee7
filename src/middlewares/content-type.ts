import {Request, Response, NextFunction} from "express";
import createError from "http-errors";

const message = (type: string)=>`Content-Type ${type} not supported in body. Only application/json and application/x-www-form-urlencoded bodies are supported`;

export default function contentType(req: Request, _res: Response, next: NextFunction) {
    if (parseInt(req.get("content-length") as string) && !req.is(["json", "urlencoded"]))
        return next(createError(415, message(req.get("content-type") as string)));
    return next();
}
