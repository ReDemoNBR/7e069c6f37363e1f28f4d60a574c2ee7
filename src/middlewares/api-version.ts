import {Request, Response, NextFunction} from "express";
import {API_HEADER_NAME, API_HEADER_VALUE} from "../../env";

export default function apiVersion(_req: Request, res: Response, next: NextFunction) {
    res.set(API_HEADER_NAME, API_HEADER_VALUE);
    return next();
}
