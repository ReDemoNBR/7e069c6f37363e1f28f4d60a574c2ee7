import {Logger} from "winston";

export type LoggerType = Logger & {
    morganHttpStream: {
        write: (message: any)=>Logger.Http
    }
}
