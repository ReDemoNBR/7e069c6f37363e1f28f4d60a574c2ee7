import Winston from "winston";
import {LOG_LEVEL} from "../../env";

export default new Winston.transports.Console({level: LOG_LEVEL});
