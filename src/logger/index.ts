import Winston from "winston";
import ConsoleTransport from "./console";
import {LoggerType} from "./type";

const {createLogger, format} = Winston;
const {combine, colorize, errors, simple, splat, timestamp} = format;

const Logger = createLogger({
    transports: [
        ConsoleTransport
    ],
    format: combine(colorize(), errors({stack: true}), timestamp(), splat(), simple())
}) as LoggerType;

Logger.morganHttpStream = {
    write: (message: any)=>Logger.http(message)
};

export default Logger;
