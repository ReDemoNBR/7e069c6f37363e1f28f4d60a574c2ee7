// WORKER THREAD
import {Server} from "http";
import {AddressInfo} from "net";
import {Application} from "express";
import {SERVER_API_PORT} from "../env";
import Logger from "./logger";
import serverConfig from "./server-config";
import gracefulExit from "./graceful-exit";
import initDependencies from "./init-dependencies";

export default async(PORT=SERVER_API_PORT)=>{
    await initDependencies();
    // eslint-disable-next-line node/no-unsupported-features/es-syntax
    const {default: app}: {default: Application} = await import("./app");
    return app.listen(PORT, function logPort(this: Server) {
        const server = serverConfig(this);
        process.on("SIGTERM", ()=>gracefulExit(server));
        process.on("SIGINT", ()=>gracefulExit(server));
        Logger.info(`Server open on port ${(this.address() as AddressInfo).port}`);
    });
};
