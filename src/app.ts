import Express from "express";
import compression from "compression";
import cors from "cors";
import helmet from "helmet";
import frameguard from "frameguard";
import referrerPolicy from "referrer-policy";
import appConfig from "./app-config";
import {MAX_REQUEST_BODY_SIZE} from "../env";
import routes from "./routes";

const app = Express();

// body parsers
app.use(Express.json({limit: MAX_REQUEST_BODY_SIZE}));
app.use(Express.urlencoded({extended: false, limit: MAX_REQUEST_BODY_SIZE}));

// compress response bodies
app.use(compression());

// allow CORS
app.use(cors({
    exposedHeaders: ["Authorization"]
}));

// security headers
app.use(helmet());
app.use(frameguard({action: "deny"}));
app.use(referrerPolicy({policy: "same-origin"}));

appConfig(app);

// Routes
app.use(routes);

export default app;
