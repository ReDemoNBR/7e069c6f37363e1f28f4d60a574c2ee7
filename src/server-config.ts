import {Server} from "http";
import {API_TIMEOUT} from "../env";

export default (server: Server)=>{
    server.setTimeout(API_TIMEOUT);
    return server;
};
