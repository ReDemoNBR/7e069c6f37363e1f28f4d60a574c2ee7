import {Router} from "express";
// import Swagger from "swagger-ui-express";
import ErrorHandler from "../middlewares/error-handler";

import clusterLog from "../middlewares/cluster-log";
import morgan from "../middlewares/morgan";
import apiVersion from "../middlewares/api-version";
import accepts from "../middlewares/accepts";
import contentType from "../middlewares/content-type";
import health from "./health";
import user from "./user";
// import swagger from "../swagger";

const router = Router();

router.use(clusterLog());
router.use(morgan);
router.use(apiVersion);
router.use(accepts);
router.use(contentType);

/* router.use("/docs", Swagger.serve, Swagger.setup(swagger, {
    customSiteTitle: "Redacted API Docs"
})); */
router.use("/health", health);
router.use("/user", user);

// error handlers
router.use(ErrorHandler);

export default router;
