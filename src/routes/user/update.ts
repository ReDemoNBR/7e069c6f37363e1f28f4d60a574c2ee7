import type {Request, Response, NextFunction} from "express";
import createError from "http-errors";
import {InferType} from "yup";
import User from "../../db/models/user";
import {UserCreateSchema, UserPatchSchema} from "../../schemas/user";

export default async(req: Request, res: Response, next: NextFunction)=>{
    const body = req.body as InferType<typeof UserCreateSchema | typeof UserPatchSchema>;
    const userId = req.params.userId as unknown as number;
    try {
        if (body.email) {
            const exists = Boolean(await User.findOne({attributes: ["id"], where: {email: body.email}, raw: true}));
            if (exists) return next(createError(409, "An user already exists with this email", {errors: ["user email must be unique"]}));
        }
        const [updateCount] = await User.update(body, {where: {id: userId}});
        if (!updateCount) return next(createError(404, "User not found"));
        return res.status(204).send();
    } catch(e) {
        return next(e);
    }
};
