import type {Request, Response, NextFunction} from "express";
import createError from "http-errors";
import User from "../../db/models/user";

export default async function read(req: Request, res: Response, next: NextFunction) {
    const userId = req.params.userId as unknown as number;
    try {
        const user = await User.findOne({where: {id: userId}});
        if (!user) return next(createError(404, "User not found"));
        return res.send(user);
    } catch(e) {
        return next(e);
    }
}
