import type {Request, Response, NextFunction} from "express";
import createError from "http-errors";
import {InferType} from "yup";
import {UserCreateSchema} from "../../schemas/user";
import User from "../../db/models/user";

export default async(req: Request, res: Response, next: NextFunction)=>{
    const body = req.body as InferType<typeof UserCreateSchema>;
    try {
        const exists = Boolean(await User.findOne({attributes: ["id"], where: {email: body.email}, raw: true}));
        if (exists) return next(createError(409, "An user already exists with this email", {errors: ["user email must be unique"]}));
        const user = await User.create(body);
        return res.status(201).send(user);
    } catch(e) {
        return next(e);
    }
};
