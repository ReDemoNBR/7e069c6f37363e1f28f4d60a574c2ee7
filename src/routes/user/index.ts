import {Router} from "express";
import {bodyValidator, paramsValidator} from "../../middlewares/schema-validator";
import create from "./create";
import read from "./read";
import remove from "./remove";
import update from "./update";
import {UserCreateSchema, UserPatchSchema, UserRouteParamSchema} from "../../schemas/user";

const router = Router();
const createBodyValidator = bodyValidator(UserCreateSchema);
const userIdParamValidator = paramsValidator(UserRouteParamSchema);

router.get("/:userId", userIdParamValidator, read);
router.delete("/:userId", userIdParamValidator, remove);
router.post("/", createBodyValidator, create);
// router.get("/", list);
router.patch("/:userId", userIdParamValidator, bodyValidator(UserPatchSchema), update);
router.put("/:userId", userIdParamValidator, createBodyValidator, update);

export default router;
