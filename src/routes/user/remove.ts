import type {Request, Response, NextFunction} from "express";
import createError from "http-errors";
import User from "../../db/models/user";

export default async function remove(req: Request, res: Response, next: NextFunction) {
    const userId = req.params.userId as unknown as number;
    try {
        const deleteCount = await User.destroy({where: {id: userId}, limit: 1});
        if (!deleteCount) return next(createError(404, "User not found"));
        return res.status(204).send();
    } catch(e) {
        return next(e);
    }
}
