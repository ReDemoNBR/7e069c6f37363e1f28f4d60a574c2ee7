import {Router} from "express";
import api from "./api";
import db from "./db";

const router = Router();

router.get("/api", api);
router.get("/db", db);

export default router;
