import {Request, Response} from "express";
import db from "../../db";
import Logger from "../../logger";

export default async(_req: Request, res: Response)=>{
    let status = 500;
    try {
        await db.authenticate();
        status = 204;
    } catch(_e) {
        Logger.warn(`Health check could not connect to DB`);
    } finally {
        res.status(status).send();
    }
};
