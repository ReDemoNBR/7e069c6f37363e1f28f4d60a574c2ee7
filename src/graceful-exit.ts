import {Server} from "http";
import db from "./db";

export default async(server: Server)=>{
    await db.close();
    await new Promise(resolve=>server.close(resolve));
};
