/* eslint-disable node/no-unsupported-features/es-syntax */
import Cluster from "cluster";

/** @TODO remove Cluster.isMaster when NodeJS v14 support is dropped */
if (Cluster.isPrimary || Cluster.isMaster) import("./master");
else import("./");
