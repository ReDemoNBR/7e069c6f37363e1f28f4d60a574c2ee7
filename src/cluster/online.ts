import {Worker} from "cluster";
import Logger from "../logger";

export default (worker: Worker)=>Logger.verbose(`worker ${worker.id} (pid: ${worker.process.pid}) is online`);
