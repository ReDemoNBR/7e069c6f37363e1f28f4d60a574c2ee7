import Cluster, {Worker} from "cluster";
import Logger from "../logger";

export default (worker: Worker, code: number, signal?: string)=>{
    Logger.debug(`worker ${worker.id} (pid: ${worker.process.pid}) died (signal: ${signal}; code: ${code})`);
    worker.disconnect();
    Cluster.fork();
};
