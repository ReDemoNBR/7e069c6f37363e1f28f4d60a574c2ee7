import {Worker} from "cluster";
import Logger from "../logger";

export default (worker: Worker)=>{
    Logger.debug(`worker ${worker.id} (pid: ${worker.process.pid}) was disconnected by master`);
    worker.process.kill(worker.process.pid);
    worker.disconnect();
};
