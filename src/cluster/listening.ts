import {Worker} from "cluster";
import {AddressInfo} from "net";
import Logger from "../logger";

export default (worker: Worker, address: AddressInfo)=>Logger.verbose(`worker ${worker.id} (pid: ${worker.process.pid}) is listening on port ${address.port}`);
