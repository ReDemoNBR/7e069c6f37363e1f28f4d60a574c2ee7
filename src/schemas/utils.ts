import {AnySchema} from "yup";

// eslint-disable-next-line arrow-body-style
export const patchShape = (baseShape: Record<string, AnySchema>)=>{
    // eslint-disable-next-line unicorn/prefer-object-from-entries,
    return Object.entries(baseShape).reduce<typeof baseShape>((obj, [key, value])=>({...obj, [key]: value.clone().notRequired()}), {});
};
