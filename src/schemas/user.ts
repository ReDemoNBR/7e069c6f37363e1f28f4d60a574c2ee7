import {date, number, string, object} from "yup";
import {patchShape} from "./utils";

const addressSchema = object({
    streetName: string().trim().required(),
    houseNumber: string().trim().required(),
    city: string().trim().required(),
    state: string().trim().length(2).required()
});

const baseShape = {
    id: number().integer().positive().required(),
    email: string().trim().email().required(),
    name: string().trim().required(),
    phone: string().trim().notRequired().nullable(),
    address: addressSchema.required().default(undefined),
    created: date().required(),
    updated: date().required()
};

const omitKeys: ("id" | "created" | "updated")[] = ["id", "created", "updated"];
patchShape(baseShape);

export const UserDBSchema = object(baseShape);
export const UserCreateSchema = UserDBSchema.omit(omitKeys).shape({
    phone: baseShape.phone.clone().default(null),
    address: baseShape.address.clone().default(undefined)
}).required();
export const UserPatchSchema = object(patchShape(baseShape)).omit(omitKeys).required();
export const UserRouteParamSchema = object({userId: number().integer().positive().required()}).required();
