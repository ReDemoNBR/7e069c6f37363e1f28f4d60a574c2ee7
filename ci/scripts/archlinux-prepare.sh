#!/usr/bin/env bash
## this is running as root

sed --in-place "s/^#\(en_US.UTF-8 UTF-8\)/\1/" /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" > locale.conf
pacman-key --init
pacman-key --populate
ln --symbolic --force /usr/share/zoneinfo/Etc/UTC /etc/localtime

pacman -Syu --noconfirm

pacman -Sy --noconfirm --needed reflector
reflector --save /etc/pacman.d/mirrorlist --sort score
pacman -Syu --noconfirm --needed base base-devel coreutils sudo

pacman -S --noconfirm --needed nodejs npm postgresql
# As Systemd doesnt work, the directory has to be created and permissions set manually
mkdir -p /var/run/postgresql
chown --recursive postgres:postgres /var/run/postgresql

sudo -u postgres initdb -D /var/lib/postgres/data/
sudo -u postgres pg_ctl -D /var/lib/postgres/data/ -l /tmp/logfile start
sudo -u postgres psql -c "CREATE USER $POSTGRES_USER WITH PASSWORD '$POSTGRES_PASSWORD'"
sudo -u postgres createdb --owner "$POSTGRES_USER" "$POSTGRES_DB"
