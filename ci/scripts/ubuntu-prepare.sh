#!/usr/bin/env bash
## This is running as root

apt update --yes
apt upgrade --yes

# Dependencies for installing the required apps
apt install --yes --no-install-recommends sudo apt-utils curl wget tzdata gnupg-agent software-properties-common ca-certificates

# Setting timezone (set to your own preference)
ln --symbolic --force /usr/share/zoneinfo/Etc/UTC /etc/localtime
dpkg-reconfigure --frontend noninteractive tzdata

# Adding NodeSource repository
curl -L "https://deb.nodesource.com/setup_${NODE_MAJOR_VERSION}.x" | bash -

# Adding PostgreSQL repository
echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -


# Installing dependencies
apt update --yes
apt upgrade --yes
## Node and PostgreSQL
apt install --yes --no-install-recommends nodejs postgresql postgresql-contrib

# Configuring dependencies
service postgresql start
sudo -u postgres psql -c "CREATE USER $POSTGRES_USER WITH PASSWORD '$POSTGRES_PASSWORD'"
sudo -u postgres createdb --owner "$POSTGRES_USER" "$POSTGRES_DB"
