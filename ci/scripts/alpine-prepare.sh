#!/usr/bin/env sh
## This is running as root in bourne shell

apk update
apk upgrade
apk add sudo npm nodejs-current postgresql

# Configuring dependencies
mkdir -p /run/postgresql
chown postgres:postgres /run/postgresql
sudo -u postgres mkdir -p /var/lib/postgresql/data/
sudo -u postgres initdb -D /var/lib/postgresql/data/
sudo -u postgres pg_ctl start -D /var/lib/postgresql/data/
sudo -u postgres psql -c "CREATE USER $POSTGRES_USER WITH PASSWORD '$POSTGRES_PASSWORD'"
sudo -u postgres createdb --owner "$POSTGRES_USER" "$POSTGRES_DB"
