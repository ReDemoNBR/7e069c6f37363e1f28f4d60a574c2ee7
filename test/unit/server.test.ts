import {assert} from "chai";
import {resolve} from "path";
import Sinon, {SinonStub} from "sinon";
import type {Server} from "http";
// eslint-disable-next-line security/detect-child-process
import ChildProcess from "child_process";
import {callbackify} from "util";
import ms from "ms";
import appFactory from "./app.factory";
import serverConfig from "../../src/server-config";
import db from "../../src/db";
import {API_TIMEOUT} from "../../env";
import gracefulExit from "../../src/graceful-exit";

type FunctionType = (..._args: any[])=>any;

function clearEvents() {
    const sigint = process.listeners("SIGINT");
    const sigterm = process.listeners("SIGTERM");
    sigint.forEach(handler=>process.off("SIGINT", handler));
    sigterm.forEach(handler=>process.off("SIGTERM", handler));
    return [sigint, sigterm];
}

function restoreEvents(sigint: FunctionType[], sigterm: FunctionType[]) {
    sigint.forEach(handler=>process.once("SIGINT", handler));
    sigterm.forEach(handler=>process.once("SIGTERM", handler));
}

describe("HTTP Server configuration", ()=>{
    describe("Server config", ()=>{
        let server: Server, maxListeners: number;

        before(async()=>{
            maxListeners = process.getMaxListeners();
            process.setMaxListeners(Infinity);
            server = serverConfig(appFactory().listen(0));
            await new Promise(resolve=>server.once("listening", resolve));
        });

        after(()=>{
            process.setMaxListeners(maxListeners);
            server.close();
        });

        it("Should have correct request timeout", ()=>{
            assert.strictEqual(server.timeout, API_TIMEOUT);
        });
    });

    describe("Close connections to PostgreSQL and Redis", ()=>{
        let server: Server, sigint: FunctionType[], sigterm: FunctionType[];

        before(async()=>{
            server = appFactory().listen(0);
            serverConfig(server);
            server.setTimeout(1000);
            [sigint, sigterm] = clearEvents();
            await new Promise(resolve=>server.once("listening", resolve));
            Sinon.stub(db, "close");
            Sinon.stub(server, "close").callsArg(0);
        });

        after(()=>{
            (db.close as SinonStub).restore();
            (server.close as SinonStub).restore();
            if (server.listening) server.close();
            // remove the event created in beforeAll
            clearEvents();
            restoreEvents(sigint, sigterm);
        });

        it("Should close connections", function(done) {
            this.timeout(ms("10s"));
            // convert function to callback to be able to use done() function
            const gracefulExitCb = callbackify(gracefulExit);
            process.once("SIGINT", ()=>{
                gracefulExitCb(server, ()=>{
                    assert.isTrue((db.close as SinonStub).calledOnce);
                    assert.isTrue((server.close as SinonStub).calledOnce);
                    assert.isTrue((server.close as SinonStub).calledAfter(db.close as SinonStub));
                    done();
                });
            });
            process.kill(process.pid, "SIGINT");
        });
    });

    describe("Server graceful exit", ()=>{
        let child: ChildProcess.ChildProcess;

        beforeEach(async function() {
            // increased timeout due to compiling child server during execution time
            this.timeout(ms("15s"));
            child = ChildProcess.fork(resolve(__dirname, "./child-server.ts"), {
                execArgv: ["-r", "ts-node/register"],
                stdio: ["inherit", "inherit", "inherit", "ipc"]
            });
            await new Promise<void>(resolve=>child.on("message", message=>{
                if (message==="listening") resolve();
            }));
        });

        afterEach(()=>{
            if (!child.killed) child.kill("SIGKILL");
        });

        it("Should gracefully exit when sending SIGINT (interrupt signal)", done=>{
            child.once("exit", code=>{
                assert.isTrue(child.killed);
                assert.strictEqual(child.exitCode, code);
                assert.strictEqual(child.exitCode, 0);
                done();
            });
            child.kill("SIGINT");
        });

        it("Should gracefully exit when sending SIGTERM (terminate signal)", done=>{
            child.once("exit", code=>{
                assert.isTrue(child.killed);
                assert.strictEqual(child.exitCode, code);
                assert.strictEqual(child.exitCode, 0);
                done();
            });
            child.kill("SIGTERM");
        });
    });

    describe("Should receive signals", ()=>{
        let sigint: FunctionType[], sigterm: FunctionType[];

        before(()=>{
            [sigint, sigterm] = clearEvents();
        });

        after(()=>{
            restoreEvents(sigint, sigterm);
        });

        afterEach(()=>{
            // clearing signals created
            clearEvents();
        });

        it("Should receive interrupt signal", done=>{
            process.once("SIGINT", signal=>{
                assert.strictEqual(signal, "SIGINT");
                done();
            });
            process.kill(process.pid, "SIGINT");
        });

        it("Should receive terminate signal", done=>{
            process.once("SIGTERM", signal=>{
                assert.strictEqual(signal, "SIGTERM");
                done();
            });
            process.kill(process.pid, "SIGTERM");
        });
    });

    describe("Process exit calls", ()=>{
        const sandbox = Sinon.createSandbox();

        before(()=>{
            sandbox.stub(db, "close");
            sandbox.stub(global.server, "close").callsArg(0);
        });

        after(()=>{
            sandbox.restore();
        });

        afterEach(()=>{
            sandbox.resetHistory();
        });

        it("Should call functions to close connections on SIGINT", done=>{
            const handler = ()=>{
                /** @NOTE during parallel tests, these functions are called multiple times */
                if (global.parallel) {
                    assert.isTrue((db.close as SinonStub).called);
                    assert.isTrue((global.server.close as SinonStub).called);
                } else {
                    assert.isTrue((db.close as SinonStub).calledOnce);
                    assert.isTrue((global.server.close as SinonStub).calledOnce);
                }
                assert.isTrue((global.server.close as SinonStub).calledAfter(db.close as SinonStub));
                done();
            };
            (db.close as SinonStub).resetHistory();
            (global.server.close as SinonStub).resetHistory();
            process.once("SIGINT", ()=>setTimeout(handler, 10));
            process.kill(process.pid, "SIGINT");
        });

        it("Should call functions to close connections on SIGTERM", done=>{
            const handler = ()=>{
                /** @NOTE during parallel tests, these functions are called multiple times */
                if (global.parallel) {
                    assert.isTrue((db.close as SinonStub).called);
                    assert.isTrue((global.server.close as SinonStub).called);
                } else {
                    assert.isTrue((db.close as SinonStub).calledOnce);
                    assert.isTrue((global.server.close as SinonStub).calledOnce);
                }
                assert.isTrue((global.server.close as SinonStub).calledAfter(db.close as SinonStub));
                done();
            };
            process.once("SIGTERM", ()=>setTimeout(handler, 10));
            process.nextTick(()=>process.kill(process.pid, "SIGTERM"));
        });
    });
});
