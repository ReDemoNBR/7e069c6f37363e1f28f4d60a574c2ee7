import {assert} from "chai";
import request from "supertest";
import {Server} from "http";
import {Request, Response, ErrorRequestHandler} from "express";
import {v1} from "uuid";
import Bytes from "bytes";
import appFactory from "./app.factory";
import {MAX_REQUEST_BODY_SIZE} from "../../env";

describe("Express App configuration", ()=>{
    describe("Express App config", ()=>{
        const app = appFactory();
        let server: Server;

        before(async()=>{
            app.get("/", (req: Request, res: Response)=>{
                const {query} = req;
                if (!query || !Object.keys(query).length) return res.send({
                    bigintn: 50n,
                    bigint: BigInt(50),
                    number: 50,
                    string: "50"
                });
                return res.send(query);
            });
            server = app.listen(0);
            await new Promise(resolve=>server.once("listening", resolve));
        });

        after(()=>{
            server.close();
        });

        it("Should have strong eTags working", async()=>{
            const response = await request(server).get("/").expect(200);
            assert.isNotEmpty(response.get("etag"));
            const etag = response.get("etag");
            // assert that not starts with W/ (W/ means it is a weak etag)
            assert.isFalse(etag.startsWith("W/"));
            assert.isFalse(etag.startsWith("w/"));

            // assert that if sending the eTag on the header, and it matches, then the response is empty
            const {body, text} = await request(server).get("/").set("If-None-Match", etag).expect(304);
            assert.isEmpty(body);
            assert.isEmpty(text);
        });

        it("Should convert BigInt to String", async()=>{
            const {body} = await request(server).get("/").expect(200);
            const expected: any = {
                bigintn: "50",
                bigint: "50",
                number: 50,
                string: "50"
            };
            assert.deepEqual(body, expected);
            for (const key in expected) assert.strictEqual(body[key], expected[key]);
        });

        it("Should use simple parser for query string", async()=>{
            const {body} = await request(server).get("/").query({foo: "bar", baz: "qux", hello: 42, world: 3.14}).expect(200);
            const expected: any = {foo: "bar", baz: "qux", hello: "42", world: "3.14"};
            assert.deepEqual(body, expected);
            for (const key in expected) assert.strictEqual(body[key], expected[key]);
        });
    });

    describe("Express body parser", ()=>{
        const app = appFactory();
        let server: Server;

        before(async()=>{
            app.get("/",
                (_req: Request, res: Response)=>res.status(500).send({error: "endpoint should not be hit"}),
                (e: ErrorRequestHandler, _req: Request, res: Response)=>{
                    res.send(e);
                });
            server = app.listen(0);
            await new Promise(resolve=>server.once("listening", resolve));
        });

        after(()=>{
            server.close();
        });

        it("Should have request blocked due to huge JSON body", async()=>{
            const random = ()=>Buffer.allocUnsafe(Bytes("1KB")).toString("hex");
            const obj = {foo: random()};
            while (obj.foo.length < MAX_REQUEST_BODY_SIZE) obj.foo += random();
            const {body} = await request(server).post("/").send(obj).expect(413);
            assert.isEmpty(body);
        });

        it("Should have request blocked due to huge form body", async()=>{
            const params = new URLSearchParams();
            const random = ()=>Buffer.allocUnsafe(Bytes("1KB")).toString("hex");
            do {
                let i = 1000;
                while (i--) params.append(v1(), random());
            } while (params.toString().length < MAX_REQUEST_BODY_SIZE);
            const {body} = await request(server).post("/").send(params.toString()).expect(413);
            assert.isEmpty(body);
        });
    });
});
