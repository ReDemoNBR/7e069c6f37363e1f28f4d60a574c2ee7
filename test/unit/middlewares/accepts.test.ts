import {assert} from "chai";
import request from "supertest";
import type {Request, Response} from "express";
import type {Server} from "http";
import accepts from "../../../src/middlewares/accepts";
import {unhandledError} from "../../../src/middlewares/error-handler";
import appFactory from "../app.factory";

describe("Accept header Server-Client middleware content negotiator", ()=>{
    const app = appFactory();
    let server: Server;

    before(async()=>{
        app.get("/", accepts, (_req: Request, res: Response)=>res.status(204).send(), unhandledError);
        server = app.listen(0);
        await new Promise(resolve=>server.once("listening", resolve));
    });

    after(()=>{
        server.close();
    });

    it("Should invalidate clients that do not explicitly support JSON responses", async()=>{
        const {body} = await request(server).get("/").set("Accept", "application/xml").expect(406);
        assert.isTrue(body.error);
        assert.include(body.message, "accept");
        assert.match(body.message, /json/i);
    });

    it("Should allow clients if they support JSON responses", async()=>{
        const {body} = await request(server).get("/").set("Accept", "application/json").expect(204);
        assert.isEmpty(body);
    });

    it("Should allow clients if they implicitly support JSON responses", async()=>{
        const {body} = await request(server).get("/").expect(204);
        assert.isEmpty(body);
    });
});
