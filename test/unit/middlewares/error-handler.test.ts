import {assert} from "chai";
import request from "supertest";
import type {Request, Response, NextFunction} from "express";
import type {Server} from "http";
import appFactory from "../app.factory";
import errorFactory from "../error.factory";
import {unhandledError} from "../../../src/middlewares/error-handler";

describe("Error Handler middleware", ()=>{
    const app = appFactory();
    let server: Server;

    before(async()=>{
        function generateAppError(req: Request, res: Response, next: NextFunction) {
            if (req.query.status) res.status(parseInt(req.query.status as string));
            const error = req.body.error ?? "Foo bar";
            next(error);
        }
        app.post("/", generateAppError, (_req: Request, res: Response)=>res.status(204).send(), unhandledError);
        server = app.listen(0);
        await new Promise(resolve=>server.once("listening", resolve));
    });

    after(()=>{
        server.close();
    });

    it("Should get default HTTP 500 with {error, message} body", async()=>{
        const {body} = await request(server).post("/").expect(500);
        assert.deepEqual(body, {error: true, message: "Foo bar"});
    });

    it("Should get correct HTTP status when defined", async()=>{
        const status = 418;
        const error = "Hello world";
        const {body} = await request(server).post("/").query({status}).send({error}).expect(status);
        assert.deepEqual(body, {error: true, message: error});
    });

    it("Should get correct HTTP status stripping properties to avoid leaking variables and secrets", async()=>{
        const status = 418;
        const error = {property: "value", foo: "bar"};
        const {body} = await request(server).post("/").query({status}).send({error}).expect(status);
        assert.deepEqual(body, {error: true, message: "Unhandled server error"});
    });

    it("Should send correct message when defined", async()=>{
        const status = 418;
        const error = errorFactory().message;
        const {body} = await request(server).post("/").query({status}).send({error}).expect(status);
        assert.deepEqual(body, {error: true, message: error});
    });
});
