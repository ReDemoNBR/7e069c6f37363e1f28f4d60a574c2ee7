import {assert} from "chai";
import request from "supertest";
import Sinon from "sinon";
import type {Request, Response} from "express";
import type {Server} from "http";
import {boolean, MixedSchema, number, object, string} from "yup";
import appFactory from "../app.factory";
import errorFactory from "../error.factory";
import schemaValidator, {bodyValidator, headerValidator, paramsValidator, queryValidator} from "../../../src/middlewares/schema-validator";
import {unhandledError} from "../../../src/middlewares/error-handler";

const schema1 = object({
    bool: boolean().required()
}).required();

const schema2 = object({
    num: number().integer().required()
}).required();

const schema3 = object({
    str: string().required()
}).required();

describe("Schema Validator middleware", ()=>{
    describe("Generic schema validator", ()=>{
        const app = appFactory();
        let server: Server;

        before(async()=>{
            const middleware = schemaValidator("body", schema1, schema2, schema3);
            app.post("/", middleware, (req: Request, res: Response)=>res.send(req.body), unhandledError);
            server = app.listen(0);
            await new Promise(resolve=>server.once("listening", resolve));
        });

        after(()=>{
            server.close();
        });

        it("Should validate composite schema", async()=>{
            const post = {bool: true, num: 42, str: "foo"};
            const {body} = await request(server).post("/").send(post).expect(200);
            assert.deepEqual(body, post);
        });

        it("Should remove unknown keys", async()=>{
            const expected = {bool: true, num: 42, str: "foo"};
            const post = {...expected, shouldBeStripped: true};
            const {body} = await request(server).post("/").send(post).expect(200);
            assert.deepEqual(body, expected);
        });

        it("Should respond with HTTP 400 error and point the invalid key", async()=>{
            const post: any = {bool: true, str: "foo"};
            const badProperty = "num";
            post[badProperty] = "foobar";
            const {body} = await request(server).post("/").send(post).expect(400);
            assert.isTrue(body.error);
            assert.include(body.message, "invalid");
            assert.include(body.message, "body");
            assert.isArray(body.errors);
            assert.lengthOf(body.errors, 1);
            assert.include(body.errors[0], badProperty);
            assert.include(body.errors[0], "number");
            assert.include(body.errors[0], post[badProperty]);
        });

        it("Should throw a TypeError", ()=>{
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            assert.throws(()=>schemaValidator("foo", schema1), TypeError, "foo");
        });
    });

    describe("Body schema validator", ()=>{
        const app = appFactory();
        let server: Server;

        before(async()=>{
            const middleware = bodyValidator(schema1, schema2, schema3);
            app.post("/", middleware, (req: Request, res: Response)=>res.send(req.body), unhandledError);
            server = app.listen(0);
            await new Promise(resolve=>server.once("listening", resolve));
        });

        after(()=>{
            server.close();
        });

        it("Should validate composite schema", async()=>{
            const post = {bool: true, num: 42, str: "foo"};
            const {body} = await request(server).post("/").send(post).expect(200);
            assert.deepEqual(body, post);
        });

        it("Should remove unknown keys", async()=>{
            const expected = {bool: true, num: 42, str: "foo"};
            const post = {...expected, shouldBeStripped: true};
            const {body} = await request(server).post("/").send(post).expect(200);
            assert.deepEqual(body, expected);
        });

        it("Should respond with HTTP 400 error and point the invalid key", async()=>{
            const post: any = {bool: true, str: "foo"};
            const badProperty = "num";
            post[badProperty] = "foobar";
            const {body} = await request(server).post("/").send(post).expect(400);
            assert.isTrue(body.error);
            assert.include(body.message, "invalid");
            assert.include(body.message, "body");
            assert.isArray(body.errors);
            assert.lengthOf(body.errors, 1);
            assert.include(body.errors[0], badProperty);
            assert.include(body.errors[0], "number");
            assert.include(body.errors[0], post[badProperty]);
        });
    });

    describe("Body schema validator for empty body", ()=>{
        const app = appFactory();
        let server: Server;

        before(async()=>{
            const middleware = bodyValidator(object({prop: string().notRequired()}));
            app.post("/", middleware, (req: Request, res: Response)=>res.send(req.body), unhandledError);
            server = app.listen(0);
            await new Promise(resolve=>server.once("listening", resolve));
        });

        after(()=>{
            server.close();
        });

        it("Should return 400 due to empty body after validation", async()=>{
            const post = {field: true, val: 42};
            const {body} = await request(server).post("/").send(post).expect(400);
            assert.isTrue(body.error);
            assert.include(body.message, "invalid");
            assert.include(body.message, "body");
            assert.isArray(body.errors);
            assert.lengthOf(body.errors, 1);
            assert.include(body.errors[0], "body");
            assert.include(body.errors[0], "no");
            assert.include(body.errors[0], "fields");
        });
    });

    describe("Header schema validator", ()=>{
        const app = appFactory();
        let server: Server;

        before(async()=>{
            const middleware = headerValidator(schema3);
            app.get("/", middleware, (req: Request, res: Response)=>res.send(req.headers), unhandledError);
            server = app.listen(0);
            await new Promise(resolve=>server.once("listening", resolve));
        });

        after(()=>{
            server.close();
        });

        it("Should validate composite schema", async()=>{
            const {body} = await request(server).get("/").set("str", "foo").expect(200);
            assert.deepEqual(body, {str: "foo"});
        });

        it("Should remove unknown keys", async()=>{
            const {body} = await request(server).get("/").set("str", "foo").set("should-be-stripped", "stripped").expect(200);
            assert.deepEqual(body, {str: "foo"});
        });

        it("Should respond with HTTP 400 error and point the invalid key", async()=>{
            const {body} = await request(server).get("/").expect(400);
            assert.isTrue(body.error);
            assert.include(body.message, "invalid");
            assert.include(body.message, "headers");
            assert.isArray(body.errors);
            assert.lengthOf(body.errors, 1);
            assert.include(body.errors[0], "str");
            assert.include(body.errors[0], "required field");
        });
    });

    describe("Query schema validator", ()=>{
        const app = appFactory();
        let server: Server;

        before(async()=>{
            const middleware = queryValidator(schema1, schema2, schema3);
            app.get("/", middleware, (req: Request, res: Response)=>res.send(req.query), unhandledError);
            server = app.listen(0);
            await new Promise(resolve=>server.once("listening", resolve));
        });

        after(()=>{
            server.close();
        });

        it("Should validate composite schema", async()=>{
            const query = {bool: true, num: 42, str: "foo"};
            const {body} = await request(server).get("/").query(query).expect(200);
            assert.deepEqual(body, query);
        });

        it("Should remove unknown keys", async()=>{
            const expected = {bool: true, num: 42, str: "foo"};
            const query = {...expected, shouldBeStripped: true};
            const {body} = await request(server).get("/").query(query).expect(200);
            assert.deepEqual(body, expected);
        });

        it("Should respond with HTTP 400 error and point the invalid key", async()=>{
            const query: any = {bool: true, str: "foo"};
            const badProperty = "num";
            query[badProperty] = "foobar";
            const {body} = await request(server).get("/").query(query).expect(400);
            assert.isTrue(body.error);
            assert.include(body.message, "invalid");
            assert.include(body.message, "query");
            assert.isArray(body.errors);
            assert.lengthOf(body.errors, 1);
            assert.include(body.errors[0], badProperty);
            assert.include(body.errors[0], "number");
            assert.include(body.errors[0], query[badProperty]);
        });
    });

    describe("Route Params schema validator", ()=>{
        const app = appFactory();
        let server: Server;
        const endpointDeclaration = "/:bool/:num/:str/:stripped";

        before(async()=>{
            const middleware = paramsValidator(schema1, schema2, schema3);
            app.get(endpointDeclaration, middleware, (req: Request, res: Response)=>res.send(req.params), unhandledError);
            server = app.listen(0);
            await new Promise(resolve=>server.once("listening", resolve));
        });

        after(()=>{
            server.close();
        });

        it("Should validate composite schema", async()=>{
            const expected = {bool: true, num: 42, str: "foo"};
            const path = [expected.bool, expected.num, expected.str, "ignored"].join("/");
            const {body} = await request(server).get(`/${path}`).expect(200);
            assert.deepEqual(body, expected);
        });

        it("Should remove unknown keys", async()=>{
            const expected = {bool: true, num: 42, str: "foo"};
            const path = [expected.bool, expected.num, expected.str, "ignored"].join("/");
            const {body} = await request(server).get(`/${path}`).expect(200);
            assert.deepEqual(body, expected);
        });

        it("Should respond with HTTP 400 error and point the invalid key", async()=>{
            const expected: any = {bool: true, str: "foo"};
            const badProperty = "num";
            expected[badProperty] = "foobar";
            const path = [expected.bool, expected.num, expected.str, "ignored"].join("/");
            const {body} = await request(server).get(`/${path}`).expect(400);
            assert.isTrue(body.error);
            assert.include(body.message, "invalid");
            assert.include(body.message, "params");
            assert.isArray(body.errors);
            assert.lengthOf(body.errors, 1);
            assert.include(body.errors[0], badProperty);
            assert.include(body.errors[0], "number");
            assert.include(body.errors[0], expected[badProperty]);
        });
    });

    describe("Unhandled error", ()=>{
        const app = appFactory();
        const error = errorFactory();
        const sandbox = Sinon.createSandbox();
        let server: Server;

        before(async()=>{
            const middleware = schemaValidator("body", {noUnknown: false}, schema1, schema2, schema3);
            app.post("/", middleware, (req: Request, res: Response)=>res.send(req.body), unhandledError);
            server = app.listen(0);
            await new Promise(resolve=>server.once("listening", resolve));
            sandbox.stub(MixedSchema.prototype, "validate").rejects(error);
        });

        after(()=>{
            sandbox.restore();
            server.close();
        });

        it("Should validate composite schema", async()=>{
            const post = {bool: true, num: 42, str: "foo"};
            const {body} = await request(server).post("/").send(post).expect(500);
            assert.deepEqual(body, {error: true, message: error.message});
        });
        // eslint-disable-next-line max-lines
    });
});
