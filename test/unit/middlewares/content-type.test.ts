import {assert} from "chai";
import request from "supertest";
import type {Request, Response} from "express";
import type {Server} from "http";
import Bytes from "bytes";
import appFactory from "../app.factory";
import contentType from "../../../src/middlewares/content-type";
import {unhandledError} from "../../../src/middlewares/error-handler";

describe("Content-Type header Server-Client middleware content negotiator", ()=>{
    const app = appFactory();
    let server: Server;

    before(async()=>{
        app.post("/", contentType, (_req: Request, res: Response)=>res.status(204).send(), unhandledError);
        server = app.listen(0);
        await new Promise(resolve=>server.once("listening", resolve));
    });

    after(()=>{
        server.close();
    });

    it("Should invalidate clients that do not send supported bodies types", async()=>{
        const data = Buffer.allocUnsafe(Bytes("300KB"));
        const type = "application/pdf";
        const {body} = await request(server).post("/").send(data).set("Content-Type", type).expect(415);
        assert.isTrue(body.error);
        assert.include(body.message, "Content-Type");
        assert.include(body.message, "not supported in body");
        assert.include(body.message, type);
    });

    it("Should allow JSON request body", async()=>{
        const {body} = await request(server).post("/").send({foo: "bar"}).set("Content-Type", "application/json").expect(204);
        assert.isEmpty(body);
    });

    it("Should allow urlencoded request body", async()=>{
        const {body} = await request(server).post("/").send("foo=bar").set("Content-Type", "application/x-www-form-urlencoded").expect(204);
        assert.isEmpty(body);
    });
});
