import {assert} from "chai";
import request from "supertest";
import Sinon from "sinon";
import userFactory from "./user.factory";
import errorFactory from "../../error.factory";
import User from "../../../../src/db/models/user";
import {UserDBSchema} from "../../../../src/schemas/user";

type UserType = ReturnType<typeof userFactory>;

describe("User create endpoint", ()=>{
    const omitKeys: ("id" | "created" | "updated")[] = ["id", "created", "updated"];
    let user: UserType, existingUser: UserType;

    before(async()=>{
        existingUser = (await User.create(userFactory())).toJSON() as UserType;
    });

    after(async()=>{
        await User.destroy({where: {id: existingUser.id}});
    });

    beforeEach(async()=>{
        user = (await User.create(userFactory())).toJSON() as UserType;
    });

    afterEach(async()=>{
        await User.destroy({where: {id: user.id}});
    });

    it("Should create an user when sending all data", async()=>{
        const data = userFactory(omitKeys);
        const {body} = await request(global.server).post("/user").send(data).expect(201);
        assert.deepInclude(body, data);
        const valid = await UserDBSchema.isValid(body);
        assert.isTrue(valid);
    });

    it("Should create an user ignoring extra data sent", async()=>{
        // sends with id, created, updated fields (which are in model) and another field
        const data: any = userFactory();
        data.thisShouldBeStripped = 42;
        const {body} = await request(global.server).post("/user").send(data).expect(201);
        const expected: Record<string, any> = {...data};
        ["id", "created", "updated", "thisShouldBeStripped"].forEach(key=>{
            assert.notDeepEqual(body[key], data[key]);
            delete expected[key]; // remove property for comparing to body
        });
        assert.notProperty(body, "thisShouldBeStripped");
        assert.deepInclude(body, expected);
        const valid = await UserDBSchema.isValid(body);
        assert.isTrue(valid);
    });

    it("Should create an user when sending minimal data", async()=>{
        const data = userFactory([...omitKeys, "phone"]);
        const {body} = await request(global.server).post("/user").send(data).expect(201);
        assert.deepInclude(body, data);
        assert.isNull(body.phone);
        const valid = await UserDBSchema.isValid(body);
        assert.isTrue(valid);
    });

    it("Should not create an user when email already exists", async()=>{
        const data = {...userFactory(omitKeys), email: existingUser.email};
        const {body} = await request(global.server).post("/user").send(data).expect(409);
        assert.isTrue(body.error);
        assert.include(body.message, "user");
        assert.include(body.message, "email");
        assert.include(body.message, "exists");
        assert.isArray(body.errors);
        assert.lengthOf(body.errors, 1);
        assert.include(body.errors[0], "email");
        assert.include(body.errors[0], "unique");
    });

    it("Should not create user when there are missing required fields", async()=>{
        const data = userFactory([...omitKeys, "name"]);
        const {body} = await request(global.server).post("/user").send(data).expect(400);
        assert.isTrue(body.error);
        assert.include(body.message, "invalid");
        assert.include(body.message, "body");
        assert.isArray(body.errors);
        assert.lengthOf(body.errors, 1);
        assert.include(body.errors[0], "name");
        assert.include(body.errors[0], "required field");
    });

    describe("Unhandled errors", ()=>{
        const error = errorFactory();
        const sandbox = Sinon.createSandbox();

        beforeEach(()=>{
            sandbox.stub(User, "create").throws(error);
        });

        afterEach(()=>{
            sandbox.restore();
        });

        it("Should get HTTP 500", async()=>{
            const data = userFactory(omitKeys);
            const {body} = await request(global.server).post("/user").send(data).expect(500);
            assert.deepEqual(body, {error: true, message: error.message});
        });
    });
});
