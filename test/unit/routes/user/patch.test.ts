import {assert} from "chai";
import request from "supertest";
import Sinon from "sinon";
import {datatype} from "faker";
import userFactory from "./user.factory";
import errorFactory from "../../error.factory";
import User from "../../../../src/db/models/user";

type UserType = ReturnType<typeof userFactory>;

describe("User patch (update) endpoint", ()=>{
    const omitKeys: ("id" | "created" | "updated")[] = ["id", "created", "updated"];
    let user: UserType, existingUser: UserType;

    before(async()=>{
        existingUser = (await User.create(userFactory())).toJSON() as UserType;
    });

    after(async()=>{
        await User.destroy({where: {id: existingUser.id!}});
    });

    beforeEach(async()=>{
        user = (await User.create(userFactory())).toJSON() as UserType;
    });

    afterEach(async()=>{
        await User.destroy({where: {id: user.id!}});
    });

    it("Should update an user when sending all fields", async()=>{
        const data = userFactory(omitKeys) as Omit<UserType, typeof omitKeys[number]>;
        const {body} = await request(global.server).patch(`/user/${user.id}`).send(data).expect(204);
        assert.isEmpty(body);
        const userDB = (await User.findOne({where: {id: user.id!}}))!.toJSON();
        assert.deepInclude(userDB, data);
    });

    it("Should update an user ignoring extra data sent", async()=>{
        // sends with id, created, updated fields (which are in model) and another field
        const data: Record<string, any> = {
            ...userFactory(),
            thisShouldBeStripped: 42
        };
        const {body} = await request(global.server).patch(`/user/${user.id}`).send(data).expect(204);
        assert.isEmpty(body);
        const userDB: Record<string, any> = (await User.findOne({where: {id: user.id!}}))!.toJSON();
        const expected: Record<string, any> = {...data};
        ["id", "created", "updated", "thisShouldBeStripped"].forEach(key=>{
            assert.notDeepEqual(userDB[key], data[key]);
            delete expected[key]; // remove property for comparing to body
        });
        assert.notProperty(userDB, "thisShouldBeStripped");
        assert.deepInclude(userDB, expected);
    });

    it("Should update an user when sending only one field, maintaining old values", async()=>{
        const data = {name: userFactory().name};
        const oldUserDB: Record<string, any> = (await User.findOne({where: {id: user.id}}))!.toJSON();
        const {body} = await request(global.server).patch(`/user/${user.id}`).send(data).expect(204);
        assert.isEmpty(body);
        const newUserDB: Record<string, any> = (await User.findOne({where: {id: user.id}}))!.toJSON();
        assert.strictEqual(newUserDB.name, data.name);
        ["id", "title", "value", "type"].forEach(key=>{
            assert.strictEqual(oldUserDB[key], newUserDB[key]);
        });
    });

    it("Should update a contact when sending only email (that has to be unique), mantaining old values", async()=>{
        const data = {email: userFactory().email};
        const oldUserDB: Record<string, any> = (await User.findOne({where: {id: user.id}}))!.toJSON();
        const {body} = await request(global.server).patch(`/user/${user.id}`).send(data).expect(204);
        assert.isEmpty(body);
        const newUserDB: Record<string, any> = (await User.findOne({where: {id: user.id}}))!.toJSON();
        assert.strictEqual(newUserDB.email, data.email);
        ["id", "value", "type", "description"].forEach(key=>{
            assert.strictEqual(oldUserDB[key], newUserDB[key]);
        });
    });

    it("Should not update an user if does not exist", async()=>{
        const data = userFactory();
        // loop to find an ID that does not exist
        let userId: number;
        do userId = datatype.number(2**53);
        while (await User.findOne({attributes: ["id"], where: {id: userId}}));
        const {body} = await request(global.server).patch(`/user/${userId}`).send(data).expect(404);
        assert.isTrue(body.error);
        assert.include(body.message, "User");
        assert.include(body.message, "not found");
    });

    it("Should not read user when sending invalid number to route params", async()=>{
        const data = userFactory();
        const {body} = await request(global.server).patch("/user/notnumber").send(data).expect(400);
        assert.isTrue(body.error);
        assert.include(body.message, "invalid");
        assert.include(body.message, "params");
        assert.isArray(body.errors);
        assert.lengthOf(body.errors, 1);
        assert.include(body.errors[0], "userId");
        assert.include(body.errors[0], "number");
        assert.include(body.errors[0], "type");
    });

    it("Should not update an user when email already exists", async()=>{
        // forcing email of the user to equal an existing email
        const data = {...userFactory(), email: existingUser.email};
        const {body} = await request(global.server).patch(`/user/${user.id}`).send(data).expect(409);
        assert.isTrue(body.error);
        assert.include(body.message, "user");
        assert.include(body.message, "email");
        assert.include(body.message, "exists");
        assert.isArray(body.errors);
        assert.lengthOf(body.errors, 1);
        assert.include(body.errors[0], "email");
        assert.include(body.errors[0], "unique");
    });

    describe("Unhandled errors", ()=>{
        const error = errorFactory();
        const sandbox = Sinon.createSandbox();

        before(()=>{
            sandbox.stub(User, "update").throws(error);
        });

        after(()=>{
            sandbox.restore();
        });

        it("Should get HTTP 500 with unhandled errors", async()=>{
            const data = userFactory();
            const {body} = await request(global.server).patch(`/user/${user.id}`).send(data).expect(500);
            assert.deepEqual(body, {error: true, message: error.message});
        });
    });
});
