import {assert} from "chai";
import request from "supertest";
import Sinon from "sinon";
import {datatype} from "faker";
import userFactory from "./user.factory";
import errorFactory from "../../error.factory";
import User from "../../../../src/db/models/user";

describe("User delete/remove endpoint", ()=>{
    let user: ReturnType<typeof userFactory>;

    beforeEach(async()=>{
        user = (await User.create(userFactory())).toJSON() as ReturnType<typeof userFactory>;
    });

    afterEach(async()=>{
        if (!("restore" in User.destroy)) await User.destroy({where: {id: user.id!}});
    });

    it("Should delete user", async()=>{
        const {body} = await request(global.server).delete(`/user/${user.id}`).expect(204);
        assert.isEmpty(body);
    });

    it("Should not delete user when sending invalid number to route params", async()=>{
        const {body} = await request(global.server).delete("/user/notnumber").expect(400);
        assert.isTrue(body.error);
        assert.include(body.message, "invalid");
        assert.include(body.message, "params");
        assert.isArray(body.errors);
        assert.lengthOf(body.errors, 1);
        assert.include(body.errors[0], "userId");
        assert.include(body.errors[0], "number");
        assert.include(body.errors[0], "type");
    });

    it("Should not delete user that does not exist", async()=>{
        // loop to find an ID that does not exist
        let userId: number;
        do userId = datatype.number(2**53);
        while (await User.findOne({attributes: ["id"], where: {id: userId}}));

        const {body} = await request(global.server).delete(`/user/${userId}`).expect(404);
        assert.isTrue(body.error);
        assert.include(body.message, "User");
        assert.include(body.message, "not found");
    });

    describe("Unhandled errors", ()=>{
        const error = errorFactory();
        const sandbox = Sinon.createSandbox();

        before(()=>{
            sandbox.stub(User, "destroy").throws(error);
        });

        after(()=>{
            sandbox.restore();
        });

        it("Should get HTTP 500 with unhandled errors", async()=>{
            const {body} = await request(global.server).delete(`/user/${user.id}`).expect(500);
            assert.deepEqual(body, {error: true, message: error.message});
        });
    });
});
