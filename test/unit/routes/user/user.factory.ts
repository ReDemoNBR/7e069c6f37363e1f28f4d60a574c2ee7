import {address, datatype, date, internet, name, phone} from "faker";
import omit from "lodash/omit.js";

const generateRandomUser = ()=>({
    id: datatype.number(2**53),
    email: internet.email(),
    name: [name.firstName(), name.middleName(), name.lastName()].join(" "),
    phone: phone.phoneNumber("+1 (###) ###-####"),
    address: {
        streetName: address.streetName(),
        houseNumber: datatype.number(9999).toString(),
        city: address.city(),
        state: address.stateAbbr()
    },
    created: date.past(20),
    updated: date.recent(60)
});

export default (keysToOmit: string[] = [])=>omit(generateRandomUser(), keysToOmit);
