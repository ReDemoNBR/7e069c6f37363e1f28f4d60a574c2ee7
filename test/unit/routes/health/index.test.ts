import {assert} from "chai";
import request from "supertest";
import Sinon from "sinon";
import errorFactory from "../../error.factory";
import db from "../../../../src/db";

describe("Health routes", ()=>{
    it("Should get OK status from DB", async()=>{
        const {body} = await request(global.server).get("/health/db").expect(204);
        assert.isEmpty(body);
    });

    it("Should get OK status from API service", async()=>{
        const {body} = await request(global.server).get("/health/api").expect(204);
        assert.isEmpty(body);
    });

    describe("Exception handler", ()=>{
        const sandbox = Sinon.createSandbox();

        before(()=>{
            sandbox.stub(db, "authenticate").throws(errorFactory());
        });

        after(()=>{
            sandbox.restore();
        });

        it("Should return HTTP 500 if DB can not be connected", async()=>{
            const {body} = await request(global.server).get("/health/db").expect(500);
            assert.isEmpty(body);
        });
    });
});
