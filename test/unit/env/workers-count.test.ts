import {assert} from "chai";
import OS from "os";
import Sinon from "sinon";
import workersCount from "../../../env/workers-count";

describe("Workers count handler", ()=>{
    // defaults to 1 due to some issues with Android that cant detect number of threads available
    const maxThreads = OS.cpus().length || 1;

    it("Should have exact 12 workers", ()=>{
        const workers = workersCount(12);
        assert.strictEqual(workers, 12);
        const workersString = workersCount("12");
        assert.strictEqual(workersString, workers);
    });

    it("Should have up to 2 workers, if hardware has available", ()=>{
        const workers = workersCount("upto-2");
        assert.isAtMost(workers, 2);
        // no more threads than machine has available
        assert.isAtMost(workers, maxThreads);
        assert.isAtLeast(workers, 1);
        const caps = workersCount("UPTO-2");
        assert.strictEqual(caps, workers);
    });

    it("Should allocate all threads available", ()=>{
        const workers = workersCount("all");
        assert.strictEqual(workers, maxThreads);
        const caps = workersCount("ALL");
        assert.strictEqual(caps, workers);
    });

    it("Should default to 'up to 4' configuration", ()=>{
        const workers = workersCount();
        assert.isAtMost(workers, 4);
        assert.isAtMost(workers, maxThreads);
        assert.isAtLeast(workers, 1);
    });

    it("Should default to 'up to 4' configuration if send anything unhandled", ()=>{
        const workers = workersCount("foo bar baz qux quux");
        assert.isAtMost(workers, 4);
        assert.isAtMost(workers, maxThreads);
        assert.isAtLeast(workers, 1);
    });

    it("Should default to 'up to 4' configuration if send anything unhandled on 'upto-' prefix", ()=>{
        const workers = workersCount("upto-foo");
        assert.isAtMost(workers, 4);
        assert.isAtMost(workers, maxThreads);
        assert.isAtLeast(workers, 1);
    });

    describe("Simulate Android behavior that returns an empty list of CPUs", ()=>{
        const sandbox = Sinon.createSandbox();

        before(()=>{
            sandbox.stub(OS, "cpus").returns([]);
        });

        after(()=>{
            sandbox.restore();
        });

        it("Should get 1 thread if OS module doesn't detect number of CPUs", ()=>{
            const workers = workersCount("upto-4");
            assert.strictEqual(workers, 1);
        });
    });
});
