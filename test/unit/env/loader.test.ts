import {assert} from "chai";
import dotenv from "dotenv";
import {resolve} from "path";
import Sinon from "sinon";
import envLoader from "../../../env/loader";
import envFactory from "./env.factory";
import EnvSchema from "./env-schema";

describe("Environment loading", ()=>{
    const {parsed: parsedEnv} = dotenv.config({path: resolve(__dirname, "../../../sample.env")});
    describe("Loading environment sampled in project (sample.env)", ()=>{
        const sandbox = Sinon.createSandbox();

        before(()=>{
            sandbox.stub(process, "env").value(parsedEnv);
        });

        after(()=>{
            sandbox.restore();
        });

        it("Should be a frozen object", ()=>{
            const env = envLoader();
            const frozen = Object.isFrozen(env);
            assert.isTrue(frozen);
        });

        it("Should have correct schema", async()=>{
            const env = envLoader();
            const valid = await EnvSchema.isValid(env);
            assert.isTrue(valid);
        });
    });

    describe("Loading empty environment", ()=>{
        const sandbox = Sinon.createSandbox();

        before(()=>{
            sandbox.stub(process, "env").value({});
        });

        after(()=>{
            sandbox.restore();
        });

        it("Should be a frozen object", ()=>{
            const env = envLoader();
            const frozen = Object.isFrozen(env);
            assert.isTrue(frozen);
        });

        it("Should have correct schema", async()=>{
            const env = envLoader();
            const valid = await EnvSchema.isValid(env);
            assert.isTrue(valid);
        });
    });

    describe("Loading a factory environment", ()=>{
        const sandbox = Sinon.createSandbox();

        beforeEach(()=>{
            const environment = envFactory();
            delete environment.PROD;
            // for (const key in environment) sandbox.stub(process.env, key).value(environment[key]);
            sandbox.stub(process, "env").value(environment);
        });

        afterEach(()=>{
            sandbox.restore();
        });

        it("Should be a frozen object", ()=>{
            const env = envLoader();
            const frozen = Object.isFrozen(env);
            assert.isTrue(frozen);
        });

        it("Should have correct schema", async()=>{
            const env = envLoader();
            const valid = await EnvSchema.isValid(env);
            assert.isTrue(valid);
        });
    });
});
