import {assert} from "chai";
import env from "../../../env";
import EnvSchema from "./env-schema";

describe("Environment configuration", ()=>{
    it("Should be a frozen object", ()=>{
        const frozen = Object.isFrozen(env);
        assert.isTrue(frozen);
        const extensible = Object.isExtensible(env);
        assert.isFalse(extensible);
    });

    it("Should have correct schema", async()=>{
        const valid = await EnvSchema.isValid(env);
        assert.isTrue(valid);
    });
});
