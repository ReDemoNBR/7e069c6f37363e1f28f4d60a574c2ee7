import {datatype, hacker, internet, random, system} from "faker";
import Winston from "winston";

export default ()=>{
    const environment: Record<string, any> = {
        POSTGRES_HOST: internet.ip(),
        POSTGRES_PORT: internet.port(),
        POSTGRES_DB: hacker.noun(),
        POSTGRES_USER: hacker.noun(),
        POSTGRES_PASSWORD: random.alphaNumeric(64),
        DB_MIN_CONNECTIONS: datatype.number({min: 1, max: 10}),
        DB_MAX_CONNECTIONS: datatype.number({min: 10, max: 30}),
        DB_IDLE_TIME: datatype.number({min: 1000, max: 60_000}),
        DB_ACQUIRE_TIME: datatype.number({min: 1000, max: 60_000}),
        DB_CHECK_INTERVAL_CONNECTIONS: datatype.number({min: 1000, max: 5000}),

        SERVER_API_HOST: `${internet.protocol()}://${internet.domainName()}`,
        SERVER_API_PORT: internet.port(),
        DEFAULT_LIMIT: datatype.number({min: 10, max: 100, precision: 10}),
        DEFAULT_MAX_LIMIT: datatype.number({min: 100, max: 1000, precision: 100}),
        API_HEADER_NAME: random.alphaNumeric(20),
        MAX_REQUEST_BODY_SIZE: datatype.number({min: 10_000, max: 500_000}),
        API_HEADER_VALUE: system.semver(),
        API_TIMEOUT: datatype.number({min: 10_000, max: 60_000}),

        PROCESS_WORKERS_COUNT: datatype.number({min: 1, max: 64}),
        NODE_ENV: random.arrayElement(["development", "production", "test", "staging"]),
        NODE_CLUSTER_SCHED_POLICY: random.arrayElement(["rr", "none"]),
        TZ: "Etc/UTC",
        PROD: datatype.boolean(),
        LOG_LEVEL: random.arrayElement(Object.keys(Winston.config.npm.levels))
    };
    const env: Record<string, string> = {};
    for (const key in environment) env[key] = environment[key].toString();
    return env;
};
