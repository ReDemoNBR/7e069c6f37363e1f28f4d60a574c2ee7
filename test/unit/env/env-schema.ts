import {boolean, number, object, string} from "yup";
import Winston from "winston";
import semver from "semver";

export default object({
    POSTGRES_HOST: string().required(),
    POSTGRES_PORT: number().integer().positive().required(),
    POSTGRES_DB: string().required(),
    POSTGRES_USER: string().required(),
    POSTGRES_PASSWORD: string().required(),
    DB_MIN_CONNECTIONS: number().integer().positive().required(),
    DB_MAX_CONNECTIONS: number().integer().positive().required(),
    DB_IDLE_TIME: number().integer().positive().required(),
    DB_ACQUIRE_TIME: number().integer().positive().required(),
    DB_CHECK_INTERVAL_CONNECTIONS: number().integer().positive().required(),

    SERVER_API_HOST: string().required(),
    SERVER_API_PORT: number().integer().positive().required(),
    DEFAULT_LIMIT: number().integer().positive().required(),
    DEFAULT_MAX_LIMIT: number().integer().positive().required(),
    API_HEADER_NAME: string().required(),
    MAX_REQUEST_BODY_SIZE: number().integer().positive().required(),
    // eslint-disable-next-line no-template-curly-in-string
    API_HEADER_VALUE: string().test("semver-valid", "${path} is not semver valid", (value: any)=>Boolean(semver.valid(value))).required(),
    API_TIMEOUT: number().integer().positive().required(),

    PROCESS_WORKERS_COUNT: number().integer().positive().notRequired(),
    NODE_ENV: string().notRequired(),
    NODE_CLUSTER_SCHED_POLICY: string().oneOf(["rr", "none"]).required(),
    TZ: string().required(),
    PROD: boolean().required(),
    LOG_LEVEL: string().oneOf(Object.keys(Winston.config.npm.levels)).required()
});
