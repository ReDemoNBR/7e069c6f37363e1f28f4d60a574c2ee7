import Express from "express";
import compression from "compression";
import helmet from "helmet";
import frameguard from "frameguard";
import referrerPolicy from "referrer-policy";
import appConfig from "../../src/app-config";
import {MAX_REQUEST_BODY_SIZE} from "../../env";

export default ()=>{
    const app = Express();

    app.use(Express.json({limit: MAX_REQUEST_BODY_SIZE}));
    app.use(Express.urlencoded({extended: false, limit: MAX_REQUEST_BODY_SIZE}));
    app.use(compression());
    app.use(helmet());
    app.use(frameguard({action: "deny"}));
    app.use(referrerPolicy({policy: "same-origin"}));

    appConfig(app);

    return app;
};
