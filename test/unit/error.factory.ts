import {hacker} from "faker";

export default ()=>{
    const error = Error(hacker.phrase());
    error.name = hacker.noun();
    return {name: error.name, message: error.message};
};
