import appFactory from "./app.factory";
import serverConfig from "../../src/server-config";
import gracefulExit from "../../src/graceful-exit";

(async()=>{
    const app = appFactory();
    const server = serverConfig(app.listen(0));
    await new Promise(resolve=>server.once("listening", resolve));
    process.once("SIGINT", ()=>gracefulExit(server));
    process.once("SIGTERM", ()=>gracefulExit(server));
    process.send!("listening");
    return server;
})();
