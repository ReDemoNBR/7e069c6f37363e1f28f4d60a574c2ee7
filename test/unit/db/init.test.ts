import {readdirSync} from "fs";
import {resolve} from "path";
import {assert} from "chai";
import snakeCase from "lodash/snakeCase";
import camelCase from "lodash/camelCase";
import db from "../../../src/db";
import {POSTGRES_DB} from "../../../env";

const filenames = readdirSync(resolve(__dirname, "../../../src/db/models")).map(file=>file.replace(".ts", ""));

describe("DB initialization", ()=>{
    filenames.forEach(file=>{
        it(`Should have sequelize model '${file}' defined`, async()=>{
            const modelName = camelCase(file);
            const defined = db.isDefined(modelName);
            assert.isTrue(defined);
            const model = db.model(modelName);
            // test if model is defined
            assert.isOk(model);
            assert.strictEqual(model, db.models[modelName]);

            // test if table is created with correct name
            assert.strictEqual(model.getTableName(), snakeCase(file));
            const res = await db.query(`SELECT * FROM ${model.getTableName()} LIMIT 1`);
            assert.isOk(res);
        });
    });

    it("Should have the correct database name", ()=>{
        const dbName = db.getDatabaseName();
        assert.strictEqual(dbName, POSTGRES_DB);
    });

    it("Should have the correct dialect", ()=>{
        const dbName = db.getDialect();
        assert.strictEqual(dbName, "postgres");
    });
});
