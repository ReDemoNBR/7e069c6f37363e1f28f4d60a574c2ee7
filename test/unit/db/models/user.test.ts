import {assert} from "chai";
import {boolean, object, string} from "yup";
import User from "../../../../src/db/models/user";

const UserSchema = object({
    id: object({
        type: string().strict(true).uppercase().equals(["BIGINT"]).required(),
        allowNull: boolean().equals([false]).required(),
        primaryKey: boolean().equals([true]).required()
    }).required(),
    email: object({
        type: string().strict(true).uppercase().equals(["CHARACTER VARYING(255)"]).required(),
        allowNull: boolean().equals([false]).required(),
        primaryKey: boolean().equals([false]).required()
    }).required(),
    name: object({
        type: string().strict(true).uppercase().equals(["CHARACTER VARYING(255)"]).required(),
        allowNull: boolean().equals([false]).required(),
        primaryKey: boolean().equals([false]).required()
    }).required(),
    phone: object({
        type: string().strict(true).uppercase().equals(["CHARACTER VARYING(255)"]).required(),
        allowNull: boolean().equals([true]).required(),
        primaryKey: boolean().equals([false]).required()
    }).required(),
    address: object({
        type: string().strict(true).uppercase().equals(["JSON"]).required(),
        allowNull: boolean().equals([false]).required(),
        primaryKey: boolean().equals([false]).required()
    }).required(),
    created: object({
        type: string().strict(true).uppercase().equals(["TIMESTAMP WITH TIME ZONE"]).required(),
        allowNull: boolean().equals([false]).required(),
        primaryKey: boolean().equals([false]).required()
    }).required(),
    updated: object({
        type: string().strict(true).uppercase().equals(["TIMESTAMP WITH TIME ZONE"]).required(),
        allowNull: boolean().equals([false]).required(),
        primaryKey: boolean().equals([false]).required()
    }).required()
}).noUnknown().required();

describe("User DB model", ()=>{
    it("Model should have been defined with correct attributes, columns, types and contraints", async()=>{
        const description = await User.describe();
        assert.isOk(description);
        const valid = await UserSchema.isValid(description);
        assert.isTrue(valid);
    });
});
