import {assert} from "chai";
import Sinon from "sinon";
import Logger from "../../../src/logger";
import logger from "../../../src/db/logger";
import {datatype, random} from "faker";

describe("DB logger", ()=>{
    const sandbox = Sinon.createSandbox();
    let stub: Sinon.SinonStub;

    before(()=>{
        stub = sandbox.stub(Logger, "verbose");
    });

    after(()=>{
        sandbox.restore();
    });

    afterEach(()=>{
        sandbox.resetHistory();
    });

    it("Should log message", ()=>{
        logger(random.alphaNumeric(60), datatype.number(1000));
        assert.isTrue(stub.called);
        assert.isTrue(stub.calledOnce);
    });

    it("Should log message with 1 parameter", ()=>{
        logger(random.alphaNumeric(60));
        assert.isTrue(stub.called);
        assert.isTrue(stub.calledOnce);
    });
});
