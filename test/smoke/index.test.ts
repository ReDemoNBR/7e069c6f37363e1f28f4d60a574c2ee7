import {assert} from "chai";
import request from "supertest";

describe("Health routes", ()=>{
    it("Should get OK status from DB", async()=>{
        const {body} = await request(global.server).get("/health/db").expect(204);
        assert.isEmpty(body);
    });

    it("Should get OK status from API service", async()=>{
        const {body} = await request(global.server).get("/health/api").expect(204);
        assert.isEmpty(body);
    });
});
