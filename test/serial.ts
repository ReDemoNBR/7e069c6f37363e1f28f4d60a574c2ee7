import {Server} from "http";
import {Application} from "express";
import initServer from "../src/worker";
import app from "../src/app";

declare global {
    /* eslint-disable no-inner-declarations,no-var */
    var server: Server;
    var app: Application;
    var parallel: boolean;
    /* eslint-enable no-inner-declarations,no-var */
}

export const mochaGlobalSetup = async()=>{
    const server = await initServer();
    await new Promise(resolve=>server.once("listening", resolve));
    global.server = server;
    global.app = app;
    global.parallel = false;
};

export const mochaGlobalTeardown = ()=>{
    global.server.close();
};
