import {Server} from "http";
import {Application} from "express";
import initServer from "../src/worker";
import app from "../src/app";
import initDependencies from "../src/init-dependencies";

declare global {
    /* eslint-disable no-inner-declarations,no-var */
    var server: Server;
    var app: Application;
    var parallel: boolean;
    /* eslint-enable no-inner-declarations,no-var */
}

export const mochaGlobalSetup = async()=>{
    await initDependencies();
};

export const mochaHooks = {
    beforeAll: async()=>{
        // open server in a random port
        const server = await initServer(0);
        await new Promise(resolve=>server.once("listening", resolve));
        global.server = server;
        global.app = app;
        global.parallel = true;
    },
    afterAll: ()=>{
        global.server.close();
    }
};
