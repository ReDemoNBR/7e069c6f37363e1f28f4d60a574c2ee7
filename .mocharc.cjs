const ms = require("ms");
const {cpus} = require("os");

const MAX_JOBS = 8;
const MIN_JOBS = 2; // at least 2 jobs when --parallel is activated

module.exports = {
    // Rules & Behavior
    allowUncaught: false,
    asyncOnly: false,
    bail: false,
    checkLeaks: true,
    delay: false,
    exit: true,
    forbidOnly: false,
    forbidPending: false,
    global: ["server", "app", "parallel"],
    jobs: Math.max(Math.min(cpus().length, MAX_JOBS), MIN_JOBS),
    parallel: false,
    retries: 0,
    slow: ms("100 milliseconds"),
    timeout: ms("3 seconds"),
    traceWarning: false,
    ui: "bdd",

    // Reporting & Errors
    color: true,
    fullTrace: false,
    growl: false,
    inlineDiffs: false,
    reporter: "spec",
    reporterOptions: [
        "configFile=.mocha-multirc.json"
    ],

    // File Handling
    extension: [".test.ts"],
    ignore: [],
    recursive: true,
    sort: false,
    watch: false,
    watchFiles: [],
    watchIgnore: []
};
