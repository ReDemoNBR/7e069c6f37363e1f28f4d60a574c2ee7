# 7e069c6f37363e1f28f4d60a574c2ee7

## **Summary**
-   [Project Keypoints](#project-keypoints)
-   [Project Rules](#project-rules)
-   [Running the Project in Development Environment](#running-the-project-in-development-environment)
-   [Integrations](#integrations)
    -   [Sonatype OSS Index](#sonatype-oss-index)
-   [TODO](#todo)


## **Project Keypoints**
-   100% testing coverage
-   Serial and parallel testing support
-   Security checks
    -   SCA (aka OSS scan) with NPM's audit and Sonatype OSS Index
    -   ESLint plugins for security
    -   Express server app security headers with `Helmet`
    -   DevSecOps on CI pipelines
-   Dependency update checks so code never gets old
-   Onboarding tests to assure new engineers can have a development environment ready quickly
-   Lint check on pre-commit hooks
-   Reports for
    -   Linting
    -   Testing
    -   Coverage
-   Supports horizontal and also vertical scaling
    -   Vertical scaling with server app replicas using NodeJS' native Cluster
-   No `if PROD`s or `if DEV`s in codebase


## **Project Rules**
-   All project development cache must be saved in `.cache/` directory
-   Reports must be saved in `.reports/` directory
-   No `dependency injection`, no `state` and no `context`
-   No `class`. If there is a need to create a new custom object type, create a new `prototype`
-   Avoid object (and prototype) injections
-   No `monorepo`s
-   Core dependency services must be able to be executed locally
-   100% of testing coverage. Always
-   Do not use `process.env`. Use the environment wrapper in `env/`
-   Environment variables must be plain strings. No composite types like `object`s or `array`s. No `JSON.parse`-ish of any kind
-   Keep dependencies updated
-   No `if (PROD)` or any sort of `if (environment=="development")` by any means
-   Files and directory names in `kebab-case` standard


## **Running the Project in Development Environment**
Be sure to have installed the NodeJS runtime and NPM in the correct versions matching the `engines` field in `package.json`
located in the project root directory.  
If you will run this locally, you might prefer to have `docker` and `docker-compose` installed for running the service dependencies
with ease. Otherwise you will have to manually install and setup each one of them. _The sample `.env` file will connect to the local
dependencies using `docker` scripted dependencies._

1.  Install dependencies: `npm install`
2.  Create a `.env` file for environment
    -   A sample (`sample.env`) is already supplied for development that links to `docker-compose.yml` file. You can copy it (`cp sample.env .env`)
3.  Run the dependencies: `docker-compose up`
    -   You can use `-d | --detach` flag to run in a detached head: `docker-compose up -d`
    -   Sometimes, the containers are recreated and their respective data are destroyed, so if you already have the containers up-ed before, you might prefer to just start it: `docker-compose start`
4.  Start the NodeJS application
    -   Start single process with watch mode (aka _live-reload_): `npm start`
    -   Start single process: `npx ts-node src`
    -   Start cluster mode: `npx ts-node src/cluster`
5.  To confirm everything is OK, you might want to run the tests: `npm test`
6.  Done

## **Non-explicit Development Dependencies**
The project has some non-explicit development dependencies for running it locally that are **NOT** installed automatically by NPM. Some of them need to be installed and configured manually and some others skipped.
-   **Docker-Compose**: It is used not only for running service dependencies in containers, but also for linting the `docker-compose.yml` file. It is used during git's `pre-commit` hooks.
-   **Hadolint**: It is used for linting `Dockerfile`s. It is a good idea to have it installed. It is used during git's `pre-commit` hooks.
-   **Yamllint**: It is used for linting yaml (`.yaml` and `.yml`) files. It is used during git's `pre-commit` hooks.

## **Integrations**
### **Sonatype OSS Index**
This project integrates Sonatype OSS Index for scanning vulnerabilities in dependencies.
It is recommended that every developer that will use it locally should have an account to avoid issues with their API rate limits.
It uses the [`auditjs client`](https://www.npmjs.com/package/auditjs)  
For creating an account:
-   Register a free account [here](https://ossindex.sonatype.org/user/register)
-   Validate your account and sign in
-   Set the global config for auditjs by running `npx auditjs config`
    -   **Important**: Select OSS Index (default)
    -   Set your username (email)
    -   And paste your API token. API token can be found [here](https://ossindex.sonatype.org/user/settings)
    -   Select your cache path
-   Done! You can successfully run `npm run scan:ossi` or `npx auditjs ossi` to scan vulnerabilities in the project


## **TODO**
-   Update `Cluster.isMaster` -> `Cluster.isPrimary` as of **v16**
-   Implement SonarQube
