#!/bin/sh

wait_for() {
    i=0
    until [ $i -ge 10 ] ; do
        nc -z "$1" "$2" && break
        echo "waiting for ${1}:${2}"
        i=$(( i + 1 ))
        sleep 1s
    done
}

wait_for "$POSTGRES_HOST" "$POSTGRES_PORT"

exec "$@"
